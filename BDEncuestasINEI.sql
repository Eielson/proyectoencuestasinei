USE [master]
GO
/****** Object:  Database [EncuestasINEI]    Script Date: 21/11/2017 21:07:17 ******/
CREATE DATABASE [EncuestasINEI]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EncuestasINEI', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\EncuestasINEI.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EncuestasINEI_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\EncuestasINEI_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EncuestasINEI] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EncuestasINEI].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EncuestasINEI] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EncuestasINEI] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EncuestasINEI] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EncuestasINEI] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EncuestasINEI] SET ARITHABORT OFF 
GO
ALTER DATABASE [EncuestasINEI] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EncuestasINEI] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EncuestasINEI] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EncuestasINEI] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EncuestasINEI] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EncuestasINEI] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EncuestasINEI] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EncuestasINEI] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EncuestasINEI] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EncuestasINEI] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EncuestasINEI] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EncuestasINEI] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EncuestasINEI] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EncuestasINEI] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EncuestasINEI] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EncuestasINEI] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EncuestasINEI] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EncuestasINEI] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [EncuestasINEI] SET  MULTI_USER 
GO
ALTER DATABASE [EncuestasINEI] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EncuestasINEI] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EncuestasINEI] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EncuestasINEI] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [EncuestasINEI] SET DELAYED_DURABILITY = DISABLED 
GO
USE [EncuestasINEI]
GO
/****** Object:  Table [dbo].[Encuesta]    Script Date: 21/11/2017 21:07:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Encuesta](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Fecha] [date] NOT NULL,
	[Costo] [decimal](5, 2) NOT NULL,
	[AlcanceNacional] [bit] NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Encuesta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pregunta]    Script Date: 21/11/2017 21:07:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pregunta](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Orden] [int] NOT NULL,
	[TipoPreguntaID] [int] NOT NULL,
	[EncuestaID] [int] NOT NULL,
 CONSTRAINT [PK_Pregunta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Respuesta]    Script Date: 21/11/2017 21:07:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Respuesta](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Usuario_EncuestaID] [int] NOT NULL,
	[PreguntaID] [int] NOT NULL,
	[Texto] [varchar](150) NOT NULL,
 CONSTRAINT [PK_Respuesta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoPregunta]    Script Date: 21/11/2017 21:07:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoPregunta](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TipoPregunta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoUsuario]    Script Date: 21/11/2017 21:07:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoUsuario](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TipoUsuario] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 21/11/2017 21:07:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[TipoUsuarioID] [int] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario_Encuesta]    Script Date: 21/11/2017 21:07:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario_Encuesta](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[EncuestaID] [int] NOT NULL,
 CONSTRAINT [PK_Usuario_Encuesta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Encuesta] ON 

INSERT [dbo].[Encuesta] ([ID], [Nombre], [Descripcion], [Fecha], [Costo], [AlcanceNacional], [Estado]) VALUES (1, N'Datos personales', N'Informacion personal del encuestado', CAST(N'2017-11-20' AS Date), CAST(100.00 AS Decimal(5, 2)), 1, 1)
INSERT [dbo].[Encuesta] ([ID], [Nombre], [Descripcion], [Fecha], [Costo], [AlcanceNacional], [Estado]) VALUES (2, N'Informacion familiar', N'Informacion de la familia del encuestado', CAST(N'2017-11-05' AS Date), CAST(90.00 AS Decimal(5, 2)), 1, 1)
INSERT [dbo].[Encuesta] ([ID], [Nombre], [Descripcion], [Fecha], [Costo], [AlcanceNacional], [Estado]) VALUES (3, N'Detalles de vivienda', N'informacion de la vivienda del encuestado', CAST(N'2017-11-02' AS Date), CAST(120.00 AS Decimal(5, 2)), 0, 1)
INSERT [dbo].[Encuesta] ([ID], [Nombre], [Descripcion], [Fecha], [Costo], [AlcanceNacional], [Estado]) VALUES (4, N'Informacion laboral', N'Detalles de la ocupacion del encuestado', CAST(N'2017-11-17' AS Date), CAST(130.00 AS Decimal(5, 2)), 0, 0)
SET IDENTITY_INSERT [dbo].[Encuesta] OFF
SET IDENTITY_INSERT [dbo].[Pregunta] ON 

INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (1, N'¿En que ciudad nacio?', 1, 1, 1)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (2, N'¿Radica usted en Peru?', 4, 3, 1)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (3, N'¿En que año nació?', 2, 2, 1)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (4, N'¿Se encuentra afiliado a algún seguro?', 10, 3, 1)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (5, N'¿Cuantos idiomas domina usted?', 3, 2, 1)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (6, N'¿Con que etnia se siente usted identificado?', 6, 1, 1)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (7, N'¿Cual es el numero de su DNI?', 8, 2, 1)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (8, N'¿Padece alguna enfermedad permanente?', 9, 3, 1)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (9, N'¿Suele viajar al exterior del pais?', 5, 3, 1)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (10, N'¿Cual es su religión?', 7, 1, 1)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (11, N'¿Con cuantas personas vive?', 1, 2, 2)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (12, N'¿Es usted jefe de familia?', 2, 3, 2)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (13, N'¿Tiene familiares en el extranjero?', 5, 3, 2)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (14, N'¿En que ciudad nació su madre?', 3, 1, 2)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (15, N'¿En que ciudad nació su padre?', 4, 1, 2)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (16, N'¿Cuantos hermanos(as) tiene?', 7, 2, 2)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (17, N'¿Tiene familiares en provincia?', 6, 3, 2)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (18, N'¿Cuantos hijos(as) vivos tiene?', 9, 2, 2)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (19, N'¿Es usted casado?', 8, 3, 2)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (20, N'¿Cuantos idiomas hablan en su familia?', 10, 2, 2)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (21, N'¿Cuantas viviendas posee?', 1, 2, 3)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (22, N'¿Cuantos años vive en su actual hogar?', 2, 2, 3)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (24, N'¿De que material son las paredes de su hogar?', 4, 1, 3)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (25, N'¿Posee servicio electrico?', 7, 3, 3)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (26, N'¿Posee servicio de agua potable?', 8, 3, 3)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (27, N'¿En que distrito vive actualmente?', 3, 1, 3)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (28, N'¿De que material son los techos de su hogar?', 5, 1, 3)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (29, N'¿De que material son los pisos de su hogar?', 6, 1, 3)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (30, N'¿Cuantas habitaciones tiene la vivienda?', 9, 2, 3)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (31, N'¿Que energía o combustible utiliza al cocinar?', 10, 1, 3)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (32, N'¿Que ocupación desempeña actualmente?', 1, 1, 4)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (33, N'¿Estuvo buscando trabajo últimamente?', 3, 3, 4)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (34, N'¿Hace cuantos años realiza esta labor?', 2, 2, 4)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (35, N'¿En que distrito se ubica su centro laboral?', 4, 1, 4)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (36, N'¿Trabaja en una empresa?', 7, 3, 4)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (37, N'¿Tienes un negocio propio?', 5, 3, 4)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (39, N'¿Cuantas horas labora?', 6, 2, 4)
INSERT [dbo].[Pregunta] ([ID], [Descripcion], [Orden], [TipoPreguntaID], [EncuestaID]) VALUES (40, N'¿Cuantas horas le toma llegar a su trabajo?', 8, 2, 4)
SET IDENTITY_INSERT [dbo].[Pregunta] OFF
SET IDENTITY_INSERT [dbo].[Respuesta] ON 

INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (1, 1, 1, N'Lima')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (2, 1, 3, N'1996')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (3, 1, 5, N'1')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (4, 1, 2, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (5, 1, 9, N'No')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (6, 1, 6, N'Mestizo')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (7, 1, 10, N'Catolico')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (8, 1, 7, N'46978462')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (9, 1, 8, N'No')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (10, 1, 4, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (11, 2, 11, N'4')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (12, 2, 12, N'No')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (13, 2, 14, N'Lima')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (14, 2, 15, N'Ayacucho')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (15, 2, 13, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (16, 2, 17, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (17, 2, 16, N'3')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (18, 2, 19, N'No')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (19, 2, 18, N'0')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (20, 2, 20, N'2')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (21, 3, 1, N'Arequipa')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (22, 3, 3, N'1980')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (23, 3, 5, N'2')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (24, 3, 2, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (25, 3, 9, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (26, 3, 6, N'Blanco')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (27, 3, 10, N'Evangelico')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (28, 3, 7, N'14698735')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (29, 3, 8, N'No')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (30, 3, 4, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (31, 4, 21, N'3')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (32, 4, 22, N'6')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (33, 4, 27, N'San Isidro')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (34, 4, 24, N'Ladrillo')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (35, 4, 28, N'Ladrillo')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (36, 4, 29, N'Ladrillo')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (37, 4, 25, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (38, 4, 26, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (39, 4, 30, N'7')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (40, 4, 31, N'Gas')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (41, 5, 1, N'Lima')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (42, 5, 3, N'1998')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (43, 5, 5, N'2')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (44, 5, 2, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (45, 5, 9, N'No')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (46, 5, 6, N'Mestizo')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (47, 5, 10, N'Catolico')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (48, 5, 7, N'74698523')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (49, 5, 8, N'No')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (50, 5, 4, N'No')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (51, 7, 1, N'Trujillo')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (52, 7, 3, N'1990')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (53, 7, 5, N'3')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (54, 7, 2, N'No')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (55, 7, 9, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (56, 7, 6, N'Mestizo')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (57, 7, 10, N'Agnositco')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (58, 7, 7, N'16497818')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (59, 7, 8, N'No')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (60, 7, 4, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (61, 8, 1, N'Cusco')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (62, 8, 3, N'1985')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (63, 8, 5, N'2')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (64, 8, 2, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (65, 8, 9, N'No')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (66, 8, 6, N'Indigena')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (67, 8, 10, N'Catolico')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (68, 8, 7, N'14697523')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (69, 8, 8, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (70, 8, 4, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (71, 9, 21, N'2')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (72, 9, 22, N'8')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (73, 9, 27, N'Chorrillos')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (74, 9, 24, N'Ladrillo')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (75, 9, 28, N'Ladrillo')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (76, 9, 29, N'Madera')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (77, 9, 25, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (78, 9, 26, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (79, 9, 30, N'4')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (80, 9, 31, N'Gas natural')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (81, 10, 11, N'2')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (82, 10, 12, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (83, 10, 14, N'Cusco')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (84, 10, 15, N'Cusco')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (85, 10, 13, N'No')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (86, 10, 17, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (87, 10, 16, N'4')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (88, 10, 19, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (89, 10, 18, N'1')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (90, 10, 20, N'2')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (91, 11, 21, N'1')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (92, 11, 22, N'20')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (93, 11, 27, N'Villa El Salvador')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (94, 11, 24, N'Ladrillo')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (95, 11, 28, N'Cemento')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (96, 11, 29, N'Cemento')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (97, 11, 25, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (98, 11, 26, N'Si')
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (99, 11, 30, N'5')
GO
INSERT [dbo].[Respuesta] ([ID], [Usuario_EncuestaID], [PreguntaID], [Texto]) VALUES (100, 11, 31, N'Gas')
SET IDENTITY_INSERT [dbo].[Respuesta] OFF
SET IDENTITY_INSERT [dbo].[TipoPregunta] ON 

INSERT [dbo].[TipoPregunta] ([ID], [Nombre]) VALUES (1, N'Texto')
INSERT [dbo].[TipoPregunta] ([ID], [Nombre]) VALUES (2, N'Numerico')
INSERT [dbo].[TipoPregunta] ([ID], [Nombre]) VALUES (3, N'Si/No')
SET IDENTITY_INSERT [dbo].[TipoPregunta] OFF
SET IDENTITY_INSERT [dbo].[TipoUsuario] ON 

INSERT [dbo].[TipoUsuario] ([ID], [Nombre], [Descripcion]) VALUES (1, N'Encuestado', N'Completa las encuestas')
INSERT [dbo].[TipoUsuario] ([ID], [Nombre], [Descripcion]) VALUES (2, N'Administrador', N'Tiene acceso total')
INSERT [dbo].[TipoUsuario] ([ID], [Nombre], [Descripcion]) VALUES (3, N'Director', N'Crea las encuestas')
SET IDENTITY_INSERT [dbo].[TipoUsuario] OFF
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([ID], [Nombre], [Username], [Password], [Email], [TipoUsuarioID]) VALUES (1, N'Juan Perez', N'jperez', N'123', N'jperez@hotmail.com', 2)
INSERT [dbo].[Usuario] ([ID], [Nombre], [Username], [Password], [Email], [TipoUsuarioID]) VALUES (2, N'Marco Paredes', N'marcop', N'123', N'marcop@hotmail.com', 3)
INSERT [dbo].[Usuario] ([ID], [Nombre], [Username], [Password], [Email], [TipoUsuarioID]) VALUES (3, N'Alonso Diaz', N'alonsod', N'123', N'alonzod@hotmail.com', 1)
INSERT [dbo].[Usuario] ([ID], [Nombre], [Username], [Password], [Email], [TipoUsuarioID]) VALUES (1002, N'Carlos Salazar', N'carloss', N'123', N'carloss@hotmail.com', 1)
INSERT [dbo].[Usuario] ([ID], [Nombre], [Username], [Password], [Email], [TipoUsuarioID]) VALUES (1004, N'Gustavo Pinto', N'gustavop', N'123', N'gustavop@hotmail.com', 1)
INSERT [dbo].[Usuario] ([ID], [Nombre], [Username], [Password], [Email], [TipoUsuarioID]) VALUES (1008, N'Sofia Meza', N'sofiam', N'123', N'sofiam@hotmail.com', 1)
INSERT [dbo].[Usuario] ([ID], [Nombre], [Username], [Password], [Email], [TipoUsuarioID]) VALUES (1009, N'Angela Barreto', N'angelab', N'123', N'angelab@hotmail.com', 1)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
SET IDENTITY_INSERT [dbo].[Usuario_Encuesta] ON 

INSERT [dbo].[Usuario_Encuesta] ([ID], [UsuarioID], [EncuestaID]) VALUES (1, 3, 1)
INSERT [dbo].[Usuario_Encuesta] ([ID], [UsuarioID], [EncuestaID]) VALUES (2, 3, 2)
INSERT [dbo].[Usuario_Encuesta] ([ID], [UsuarioID], [EncuestaID]) VALUES (3, 1002, 1)
INSERT [dbo].[Usuario_Encuesta] ([ID], [UsuarioID], [EncuestaID]) VALUES (4, 1002, 3)
INSERT [dbo].[Usuario_Encuesta] ([ID], [UsuarioID], [EncuestaID]) VALUES (5, 1004, 1)
INSERT [dbo].[Usuario_Encuesta] ([ID], [UsuarioID], [EncuestaID]) VALUES (6, 1004, 3)
INSERT [dbo].[Usuario_Encuesta] ([ID], [UsuarioID], [EncuestaID]) VALUES (7, 1008, 1)
INSERT [dbo].[Usuario_Encuesta] ([ID], [UsuarioID], [EncuestaID]) VALUES (8, 1009, 1)
INSERT [dbo].[Usuario_Encuesta] ([ID], [UsuarioID], [EncuestaID]) VALUES (9, 1009, 3)
INSERT [dbo].[Usuario_Encuesta] ([ID], [UsuarioID], [EncuestaID]) VALUES (10, 1009, 2)
INSERT [dbo].[Usuario_Encuesta] ([ID], [UsuarioID], [EncuestaID]) VALUES (11, 3, 3)
SET IDENTITY_INSERT [dbo].[Usuario_Encuesta] OFF
ALTER TABLE [dbo].[Pregunta]  WITH CHECK ADD  CONSTRAINT [FK_Pregunta_Encuesta] FOREIGN KEY([EncuestaID])
REFERENCES [dbo].[Encuesta] ([ID])
GO
ALTER TABLE [dbo].[Pregunta] CHECK CONSTRAINT [FK_Pregunta_Encuesta]
GO
ALTER TABLE [dbo].[Pregunta]  WITH CHECK ADD  CONSTRAINT [FK_Pregunta_TipoPregunta] FOREIGN KEY([TipoPreguntaID])
REFERENCES [dbo].[TipoPregunta] ([ID])
GO
ALTER TABLE [dbo].[Pregunta] CHECK CONSTRAINT [FK_Pregunta_TipoPregunta]
GO
ALTER TABLE [dbo].[Respuesta]  WITH CHECK ADD  CONSTRAINT [FK_Respuesta_Pregunta] FOREIGN KEY([PreguntaID])
REFERENCES [dbo].[Pregunta] ([ID])
GO
ALTER TABLE [dbo].[Respuesta] CHECK CONSTRAINT [FK_Respuesta_Pregunta]
GO
ALTER TABLE [dbo].[Respuesta]  WITH CHECK ADD  CONSTRAINT [FK_Respuesta_Usuario_Encuesta] FOREIGN KEY([Usuario_EncuestaID])
REFERENCES [dbo].[Usuario_Encuesta] ([ID])
GO
ALTER TABLE [dbo].[Respuesta] CHECK CONSTRAINT [FK_Respuesta_Usuario_Encuesta]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_TipoUsuario] FOREIGN KEY([TipoUsuarioID])
REFERENCES [dbo].[TipoUsuario] ([ID])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_TipoUsuario]
GO
ALTER TABLE [dbo].[Usuario_Encuesta]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Encuesta_Encuesta] FOREIGN KEY([EncuestaID])
REFERENCES [dbo].[Encuesta] ([ID])
GO
ALTER TABLE [dbo].[Usuario_Encuesta] CHECK CONSTRAINT [FK_Usuario_Encuesta_Encuesta]
GO
ALTER TABLE [dbo].[Usuario_Encuesta]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Encuesta_Usuario] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuario] ([ID])
GO
ALTER TABLE [dbo].[Usuario_Encuesta] CHECK CONSTRAINT [FK_Usuario_Encuesta_Usuario]
GO
USE [master]
GO
ALTER DATABASE [EncuestasINEI] SET  READ_WRITE 
GO
