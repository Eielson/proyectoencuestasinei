﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EncuestasINEI.Models;
using EncuestasINEI.SecurityFilters;

namespace EncuestasINEI.Controllers
{
    [LoggedOnAttribute]
    public class RespuestasController : Controller
    {
        private EncuestasINEIEntitiesContext db = new EncuestasINEIEntitiesContext();

        // GET: Respuestas
        public ActionResult Index()
        {
            var respuesta = db.Respuesta.Include(r => r.Pregunta).Include(r => r.Usuario_Encuesta);
            return View(respuesta.ToList());
        }

        // GET: Respuestas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Respuesta respuesta = db.Respuesta.Find(id);
            if (respuesta == null)
            {
                return HttpNotFound();
            }
            return View(respuesta);
        }

        // GET: Respuestas/Create
        public ActionResult Create()
        {
            ViewBag.PreguntaID = new SelectList(db.Pregunta, "ID", "Descripcion");
            ViewBag.Usuario_EncuestaID = new SelectList(db.Usuario_Encuesta, "ID", "ID");
            return View();
        }

        // POST: Respuestas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Usuario_EncuestaID,PreguntaID,Texto")] Respuesta respuesta)
        {
            if (ModelState.IsValid)
            {
                db.Respuesta.Add(respuesta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PreguntaID = new SelectList(db.Pregunta, "ID", "Descripcion", respuesta.PreguntaID);
            ViewBag.Usuario_EncuestaID = new SelectList(db.Usuario_Encuesta, "ID", "ID", respuesta.Usuario_EncuestaID);
            return View(respuesta);
        }

        // GET: Respuestas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Respuesta respuesta = db.Respuesta.Find(id);
            if (respuesta == null)
            {
                return HttpNotFound();
            }
            ViewBag.PreguntaID = new SelectList(db.Pregunta, "ID", "Descripcion", respuesta.PreguntaID);
            ViewBag.Usuario_EncuestaID = new SelectList(db.Usuario_Encuesta, "ID", "ID", respuesta.Usuario_EncuestaID);
            return View(respuesta);
        }

        // POST: Respuestas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Usuario_EncuestaID,PreguntaID,Texto")] Respuesta respuesta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(respuesta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PreguntaID = new SelectList(db.Pregunta, "ID", "Descripcion", respuesta.PreguntaID);
            ViewBag.Usuario_EncuestaID = new SelectList(db.Usuario_Encuesta, "ID", "ID", respuesta.Usuario_EncuestaID);
            return View(respuesta);
        }

        // GET: Respuestas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Respuesta respuesta = db.Respuesta.Find(id);
            if (respuesta == null)
            {
                return HttpNotFound();
            }
            return View(respuesta);
        }

        // POST: Respuestas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Respuesta respuesta = db.Respuesta.Find(id);
            db.Respuesta.Remove(respuesta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: ResolverEncuesta
        public ActionResult ResolverEncuesta(int id_encuesta)
        {
            Session["encuesta_id"] = id_encuesta;
            var query = from pre in db.Pregunta
                        where pre.EncuestaID == id_encuesta
                        orderby pre.Orden 
                        select pre;
            List<Pregunta> preguntas = query.ToList();
            ViewBag.ListaPreguntas = preguntas;
            ViewBag.PreguntaID = new SelectList(db.Pregunta, "ID", "Descripcion");
            ViewBag.Usuario_EncuestaID = new SelectList(db.Usuario_Encuesta, "ID", "ID");
            return View();       
    }
        //POST: ResolverEncuesta
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResolverEncuesta(List<Respuesta> respuestas)
        {
            if (ModelState.IsValid)
            {
                int usuarioID = ((EncuestasINEI.Models.Usuario)Session["CurrentUser"]).ID;
                int encuestaID = (int)Session["encuesta_id"];

                //Crear usuario_encuesta
                Usuario_Encuesta objUsuarioEncuesta = new Usuario_Encuesta();
                objUsuarioEncuesta.UsuarioID = usuarioID;
                objUsuarioEncuesta.EncuestaID = encuestaID;
                db.Usuario_Encuesta.Add(objUsuarioEncuesta);
                db.SaveChanges();

                //Obtener el id de usuario_encuesta
                int usuario_encuestaID = objUsuarioEncuesta.ID;

                //Guardar las respuestas
                for(int a=0; a < respuestas.Count(); a++)
                {
                    Respuesta objRespuesta = new Respuesta();
                    objRespuesta.Usuario_EncuestaID = usuario_encuestaID;
                    objRespuesta.PreguntaID = respuestas[a].PreguntaID;
                    objRespuesta.Texto = respuestas[a].Texto;
                    db.Respuesta.Add(objRespuesta);
                    db.SaveChanges();
                }            
                return RedirectToAction("EncuestasParaUsuarios", "Encuestas", new { id_encuesta = encuestaID});
            }
            //ViewBag.PreguntaID = new SelectList(db.Pregunta, "ID", "Descripcion", respuesta.PreguntaID);
            //ViewBag.Usuario_EncuestaID = new SelectList(db.Usuario_Encuesta, "ID", "ID", respuesta.Usuario_EncuestaID);
            return View(respuestas);
        }

        // GET: DetallesEncuestaResuelta
        public ActionResult DetallesEncuestaResuelta(int encuestaID, int usuarioID )
        {
            int id_usuario = usuarioID;//((EncuestasINEI.Models.Usuario)Session["CurrentUser"]).ID;

            //Obtener la encuesta de la tabla usuario_encuesta
            var encuesta = from e in db.Usuario_Encuesta.Include(en => en.Encuesta).Include(u => u.Usuario)
                           where e.UsuarioID == id_usuario && e.EncuestaID == encuestaID
                           select e.ID;
            int us_en_id = encuesta.FirstOrDefault();

            //Obtener las respuestas de la encuesta
            var query = from res in db.Respuesta.Include(r => r.Pregunta).Include(r => r.Usuario_Encuesta)
                        where res.Usuario_EncuestaID == us_en_id
                        select res;

            List<Respuesta> respuestas = query.ToList();

            return View(respuestas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
