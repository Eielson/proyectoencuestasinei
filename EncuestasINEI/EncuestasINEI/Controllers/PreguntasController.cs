﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EncuestasINEI.Models;
using EncuestasINEI.SecurityFilters;
using System.Data.SqlClient;
using System.Text;

namespace EncuestasINEI.Controllers
{
    [LoggedOnAttribute]
    public class PreguntasController : Controller
    {
        private EncuestasINEIEntitiesContext db = new EncuestasINEIEntitiesContext();
        
        // GET: Preguntas
        public ActionResult Index(int id_encuesta)
        {
            var query = from p in db.Pregunta.Include(p => p.Encuesta).Include(p => p.TipoPregunta)
                        where p.EncuestaID == id_encuesta
                        select p;
            //var pregunta = db.Pregunta.Include(p => p.Encuesta).Include(p => p.TipoPregunta);
            List<Pregunta> preguntas = query.ToList();
            Session["encuesta_id"] = id_encuesta;
            return View(preguntas);
        }

        // GET: Preguntas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pregunta pregunta = db.Pregunta.Find(id);
            if (pregunta == null)
            {
                return HttpNotFound();
            }
            return View(pregunta);
        }

        // GET: Preguntas/Create
        public ActionResult Create()
        {
            ViewBag.EncuestaID = new SelectList(db.Encuesta, "ID", "Nombre");
            ViewBag.TipoPreguntaID = new SelectList(db.TipoPregunta, "ID", "Nombre");
            return View();
        }

        // POST: Preguntas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion,Orden,TipoPreguntaID,EncuestaID")] Pregunta pregunta)
        {
            if (ModelState.IsValid)
            {
                int id = (int)Session["encuesta_id"];
                pregunta.EncuestaID = id;
                //Verificar que no se repita el orden de pregunta
                var query = from p in db.Pregunta.Include(e => e.Encuesta)
                            where p.EncuestaID == id && p.Orden == pregunta.Orden
                            select p;
                List<Pregunta> preg = query.ToList();
                if(preg.Count == 0)
                {
                    db.Pregunta.Add(pregunta);
                    db.SaveChanges();
                    return RedirectToAction("Index", new { id_encuesta = id });
                }
                else
                {
                    ViewBag.ErrorOrden = "El numero de orden ya existe. Por favor ingrese otro numero";
                }               
            }
            ViewBag.EncuestaID = new SelectList(db.Encuesta, "ID", "Nombre", pregunta.EncuestaID);
            ViewBag.TipoPreguntaID = new SelectList(db.TipoPregunta, "ID", "Nombre", pregunta.TipoPreguntaID);
            return View(pregunta);
        }
        // GET: Preguntas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pregunta pregunta = db.Pregunta.Find(id);
            if (pregunta == null)
            {
                return HttpNotFound();
            }
            ViewBag.EncuestaID = new SelectList(db.Encuesta, "ID", "Nombre", pregunta.EncuestaID);
            ViewBag.TipoPreguntaID = new SelectList(db.TipoPregunta, "ID", "Nombre", pregunta.TipoPreguntaID);
            return View(pregunta);
        }

        // POST: Preguntas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion,Orden,TipoPreguntaID,EncuestaID")] Pregunta pregunta)
        {
            if (ModelState.IsValid)
            {
                int id = (int)Session["encuesta_id"];
                pregunta.EncuestaID = id;
                db.Entry(pregunta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id_encuesta = id });
            }
            ViewBag.EncuestaID = new SelectList(db.Encuesta, "ID", "Nombre", pregunta.EncuestaID);
            ViewBag.TipoPreguntaID = new SelectList(db.TipoPregunta, "ID", "Nombre", pregunta.TipoPreguntaID);
            return View(pregunta);
        }

        // GET: Preguntas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pregunta pregunta = db.Pregunta.Find(id);
            if (pregunta == null)
            {
                return HttpNotFound();
            }
            return View(pregunta);
        }

        // POST: Preguntas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pregunta pregunta = db.Pregunta.Find(id);
            try
            {
                db.Pregunta.Remove(pregunta);
                db.SaveChanges();
                return RedirectToAction("Index", new { id_encuesta = Session["encuesta_id"] });
            }
            catch (SqlException se)
            {
                StringBuilder errorMessages = new StringBuilder();
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages.Append("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                return RedirectToAction("EliminarPregunta", "Errores", new { error = errorMessages.ToString() });
            }
            catch (Exception e)
            {
                string errorMessage = "";
                errorMessage = e.Message;
                return RedirectToAction("EliminarPregunta", "Errores", new { error = errorMessage });
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
