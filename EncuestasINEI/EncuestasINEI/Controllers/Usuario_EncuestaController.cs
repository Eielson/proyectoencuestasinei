﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EncuestasINEI.Models;
using EncuestasINEI.SecurityFilters;

namespace EncuestasINEI.Controllers
{
    [LoggedOnAttribute]
    public class Usuario_EncuestaController : Controller
    {
        private EncuestasINEIEntitiesContext db = new EncuestasINEIEntitiesContext();

        // GET: Usuario_Encuesta
        public ActionResult Index()
        {
            var usuario_Encuesta = db.Usuario_Encuesta.Include(u => u.Encuesta).Include(u => u.Usuario);
            return View(usuario_Encuesta.ToList());
        }

        // GET: Usuario_Encuesta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario_Encuesta usuario_Encuesta = db.Usuario_Encuesta.Find(id);
            if (usuario_Encuesta == null)
            {
                return HttpNotFound();
            }
            return View(usuario_Encuesta);
        }

        // GET: Usuario_Encuesta/Create
        public ActionResult Create()
        {
            ViewBag.EncuestaID = new SelectList(db.Encuesta, "ID", "Nombre");
            ViewBag.UsuarioID = new SelectList(db.Usuario, "ID", "Nombre");
            return View();
        }

        // POST: Usuario_Encuesta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,UsuarioID,EncuestaID")] Usuario_Encuesta usuario_Encuesta)
        {
            if (ModelState.IsValid)
            {
                db.Usuario_Encuesta.Add(usuario_Encuesta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EncuestaID = new SelectList(db.Encuesta, "ID", "Nombre", usuario_Encuesta.EncuestaID);
            ViewBag.UsuarioID = new SelectList(db.Usuario, "ID", "Nombre", usuario_Encuesta.UsuarioID);
            return View(usuario_Encuesta);
        }

        // GET: Usuario_Encuesta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario_Encuesta usuario_Encuesta = db.Usuario_Encuesta.Find(id);
            if (usuario_Encuesta == null)
            {
                return HttpNotFound();
            }
            ViewBag.EncuestaID = new SelectList(db.Encuesta, "ID", "Nombre", usuario_Encuesta.EncuestaID);
            ViewBag.UsuarioID = new SelectList(db.Usuario, "ID", "Nombre", usuario_Encuesta.UsuarioID);
            return View(usuario_Encuesta);
        }

        // POST: Usuario_Encuesta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,UsuarioID,EncuestaID")] Usuario_Encuesta usuario_Encuesta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuario_Encuesta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EncuestaID = new SelectList(db.Encuesta, "ID", "Nombre", usuario_Encuesta.EncuestaID);
            ViewBag.UsuarioID = new SelectList(db.Usuario, "ID", "Nombre", usuario_Encuesta.UsuarioID);
            return View(usuario_Encuesta);
        }

        // GET: Usuario_Encuesta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario_Encuesta usuario_Encuesta = db.Usuario_Encuesta.Find(id);
            if (usuario_Encuesta == null)
            {
                return HttpNotFound();
            }
            return View(usuario_Encuesta);
        }

        // POST: Usuario_Encuesta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Usuario_Encuesta usuario_Encuesta = db.Usuario_Encuesta.Find(id);
            db.Usuario_Encuesta.Remove(usuario_Encuesta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
