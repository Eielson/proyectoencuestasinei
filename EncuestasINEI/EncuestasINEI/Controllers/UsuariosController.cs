﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EncuestasINEI.Models;
using System.Collections;
using System.Web.Helpers;
using EncuestasINEI.SecurityFilters;

namespace EncuestasINEI.Controllers
{
    
    public class UsuariosController : Controller
    {
        private EncuestasINEIEntitiesContext db = new EncuestasINEIEntitiesContext();

        // GET: Usuarios
        public ActionResult Index()
        {
            int id_usuario = ((EncuestasINEI.Models.Usuario)Session["CurrentUser"]).ID;
            var query = from u in db.Usuario.Include(u => u.TipoUsuario)
                        where u.ID != id_usuario
                        select u;
            List<Usuario> usuarios = query.ToList();
            //var usuario = db.Usuario.Include(u => u.TipoUsuario);
            return View(usuarios);
        }

        // GET: Usuarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // GET: Usuarios/Login
        public ActionResult Login()
        {
            if(TempData["ErrorMessage"] != null)
            {
                ViewBag.LoginMessage = TempData["ErrorMessage"].ToString();
            }
            ViewBag.TipoUsuarioID = new SelectList(db.TipoUsuario, "ID", "Nombre");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "Username,Password")] Usuario.LoginValidation usuario)
        {
            Usuario objUsuario = null;
            if (ModelState.IsValid)
            {
                var query = from u in db.Usuario.Include("TipoUsuario")
                            where u.Username == usuario.Username
                            select u;
                objUsuario = query.FirstOrDefault();
                //Validar contraseña
                if(objUsuario != null)
                {
                    if(objUsuario.Password == usuario.Password)
                    {
                        Session["CurrentUser"] = objUsuario;
                        switch (objUsuario.TipoUsuario.Nombre.ToLower())
                        {
                            case "encuestado":
                                return RedirectToAction("EncuestasParaUsuarios", "Encuestas");
                                break;
                            case "administrador":
                                return RedirectToAction("Index", "Usuarios");
                                break;
                            case "director":
                                return RedirectToAction("Index", "Encuestas");
                                break;
                        }
                        
                    }
                    else
                    {
                        ViewBag.LoginMessage = "El usuario o contraseña es incorrecto";
                    }
                }
                else
                {
                    ViewBag.LoginMessage = "El usuario o contraseña es incorrecto";
                }
            }

            //ViewBag.TipoUsuarioID = new SelectList(db.TipoUsuario, "ID", "Nombre", usuario.TipoUsuarioID);
            return View(objUsuario);
        }

        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            Session["CurrentUser"] = null;
            Session["IDEncuesta"] = null;
            return RedirectToAction("Login", "Usuarios");
        }

        // GET: Usuarios/Create
        public ActionResult Create()
        {
            ViewBag.TipoUsuarioID = new SelectList(db.TipoUsuario, "ID", "Nombre");
            return View();
        }

        // POST: Usuarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Nombre,Username,Password,Email,TipoUsuarioID")] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Usuario.Add(usuario);
                db.SaveChanges();
                if(Session["CurrentUser"] != null)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Login");
                }
                
            }

            ViewBag.TipoUsuarioID = new SelectList(db.TipoUsuario, "ID", "Nombre", usuario.TipoUsuarioID);
            return View(usuario);
        }

        // GET: Usuarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoUsuarioID = new SelectList(db.TipoUsuario, "ID", "Nombre", usuario.TipoUsuarioID);
            return View(usuario);
        }

        // POST: Usuarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Nombre,Username,Password,Email,TipoUsuarioID")] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TipoUsuarioID = new SelectList(db.TipoUsuario, "ID", "Nombre", usuario.TipoUsuarioID);
            return View(usuario);
        }

        // GET: Usuarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Usuario usuario = db.Usuario.Find(id);
            var query1 = from ue in db.Usuario_Encuesta.Include(u => u.Usuario).Include(e => e.Encuesta)
                         where (ue.UsuarioID == id)
                         select ue;
            List<Usuario_Encuesta> usuarios_encuestas = query1.ToList();
            foreach(var item in usuarios_encuestas)
            {
                var query2 = from r in db.Respuesta.Include(ue => ue.Usuario_Encuesta).Include(p => p.Pregunta)
                             where (r.Usuario_EncuestaID == item.ID)
                             select r;
                List<Respuesta> respuestas = query2.ToList();
                foreach(var item2 in respuestas)
                {
                    db.Respuesta.Remove(item2);
                    db.SaveChanges();
                }
                db.Usuario_Encuesta.Remove(item);
                db.SaveChanges();
            }
            db.Usuario.Remove(usuario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        // GET: Usuarios/EditarUsuarioLogueado
        public ActionResult EditarUsuarioLogueado()
        {
            int id = ((EncuestasINEI.Models.Usuario)Session["CurrentUser"]).ID;
            Usuario usuario = db.Usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoUsuarioID = new SelectList(db.TipoUsuario, "ID", "Nombre", usuario.TipoUsuarioID);
            return View(usuario);
        }

        // POST: Usuarios/EditarUsuarioLogueado
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarUsuarioLogueado([Bind(Include = "ID,Nombre,Username,Password,Email,TipoUsuarioID")] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
                int id_tu = usuario.TipoUsuarioID;
                TipoUsuario tu = db.TipoUsuario.Where(u => u.ID == id_tu).FirstOrDefault();
                string nombre = tu.Nombre.ToLower();
                if (nombre == "administrador")
                {
                    return RedirectToAction("Index", "Usuarios");
                }
                
                else if(nombre == "director"){
                    return RedirectToAction("Index", "Encuestas");
                }
                else
                {
                    return RedirectToAction("EncuestasParaUsuarios", "Encuestas");
                }
            }
            ViewBag.TipoUsuarioID = new SelectList(db.TipoUsuario, "ID", "Nombre", usuario.TipoUsuarioID);
            return View(usuario);
        }

        // GET: Usuarios
        public ActionResult UsuariosPorEncuesta(int id_encuesta)
        {
            Session["IDEncuesta"] = id_encuesta;
            var query1 = from us_en in db.Usuario_Encuesta.Include(u => u.Usuario).Include(e => e.Encuesta)
                         where us_en.EncuestaID == id_encuesta
                         select us_en.UsuarioID;

            var query2 = from u in db.Usuario.Include(u => u.TipoUsuario)
                        where query1.Contains(u.ID)
                        select u;

            List<Usuario> usuarios = query2.ToList();

            //Para el nombre de la encuesta
            Encuesta en = db.Encuesta.Find(id_encuesta);
            ViewBag.NombreEncuesta = en.Nombre;

            return View(usuarios);
        }

        // GET: ActividadEncuestados
        public ActionResult ActividadEncuestados()
        {
            List<Usuario> usuarios = null;
            return View(usuarios);
        }

        //Reporte1: EncuestasRealizadasporEncuestados
        public ActionResult Reporte1()
        {
            ArrayList xValue = new ArrayList();
            ArrayList yValue = new ArrayList();

            var query = from u in db.Usuario.Include("TipoUsuario")
                        from ue in db.Usuario_Encuesta
                        where (u.TipoUsuarioID == 1) && (ue.UsuarioID == u.ID)
                        group u by u.Nombre into grp
                        select new { key = grp.Key, cnt = grp.Count() };

            query.ToList().ForEach(rs => xValue.Add(rs.key));
            query.ToList().ForEach(rs => yValue.Add(rs.cnt));

            string myTheme = @"<Chart BackColor=""Transparent"">
                                    <ChartAreas>
                                        <ChartArea Name=""Default"" BackColor=""Transparent"">
                                        </ChartArea>
                                    </ChartAreas>
                                </Chart>";

            new Chart(width: 600, height: 400, theme: myTheme).
                AddTitle("Encuestas realizadas por encuestados").
                AddSeries(chartType: "Bar", xValue: xValue, yValues: yValue).
                Write("png");
            return null;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
