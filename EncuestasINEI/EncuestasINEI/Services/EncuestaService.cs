﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EncuestasINEI.Models;
using EncuestasINEI.Controllers;

namespace EncuestasINEI.Services
{
    public class EncuestaService : IEncuestaService
    {
        EncuestasController encuestaController;
        public EncuestaService()
        {
            encuestaController = new EncuestasController();
        }

        public bool crearEncuesta(Encuesta objEncuesta)
        {
            var result = encuestaController.Create(objEncuesta) as RedirectToRouteResult;
            if (result != null)
            {
                return true;
            }
            return false;
        }

        public Encuesta detallesEncuesta(int id)
        {
            var result = encuestaController.Details(id) as ViewResult;
            if (result != null)
            {
                return (Encuesta)result.ViewData.Model;
            }
            return null;
        }

        public bool editarEncuesta(Encuesta objEncuesta)
        {
            var result = encuestaController.Edit(objEncuesta) as RedirectToRouteResult;
            if (result != null)
            {
                return true;
            }
            return false;
        }

        public bool eliminarEncuesta(int id)
        {
            var result = encuestaController.DeleteConfirmed(id) as RedirectToRouteResult;
            if (result.Equals("Index"))
            {
                return true;
            }
            return false;
        }

        public List<Encuesta> listaEncuestas()
        {
            var result = encuestaController.Index() as ViewResult;
            if (result != null)
            {
                return (List<Encuesta>)result.ViewData.Model;
            }
            return null;
        }
    }
}