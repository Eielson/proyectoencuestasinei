﻿using EncuestasINEI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EncuestasINEI.Services
{
    public interface IEncuestaService
    {   
        bool crearEncuesta(Encuesta objEncuesta);
        Encuesta detallesEncuesta(int id);
        bool editarEncuesta(Encuesta objEncuesta);
        bool eliminarEncuesta(int id);
        List<Encuesta> listaEncuestas();
    }
}
