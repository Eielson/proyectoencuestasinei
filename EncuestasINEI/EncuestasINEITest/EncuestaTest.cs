﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EncuestasINEI.Models;
using Moq;
using EncuestasINEI.Services;
using System.Collections.Generic;

namespace EncuestasINEITest
{
    [TestClass]
    public class EncuestaTest
    {
        [TestInitialize]
        public void InciarMetodo()
        {
            Console.WriteLine("Inicio del método");
        }
        [TestCleanup]
        public void FinMetodo()
        {
            Console.WriteLine("Fin del método");
        }
        [TestMethod]
        public void CrearEncuesta()
        {
            Console.WriteLine("Crear encuesta");
            //Arrange
            Encuesta objEncuesta = new Encuesta();
            var mockEncuesta = new Mock<IEncuestaService>();
            //Act
            mockEncuesta.Setup(st => st.crearEncuesta(objEncuesta)).Returns(true);
            //Assert
            Assert.AreEqual(true, mockEncuesta.Object.crearEncuesta(objEncuesta));
        }

        [TestMethod]
        public void DetallesEncuesta()
        {
            Console.WriteLine("Detalles encuesta");
            //Arrange
            var mockEncuesta = new Mock<IEncuestaService>();
            //Act
            mockEncuesta.Setup(st => st.detallesEncuesta(1)).Returns(new Encuesta());
            //Assert
            Assert.IsNotNull(mockEncuesta.Object.detallesEncuesta(1));
        }

        [TestMethod]
        public void EditarEncuesta()
        {
            Console.WriteLine("Editar encuesta");
            //Arrange
            var mockEncuesta = new Mock<IEncuestaService>();
            Encuesta objEncuesta = new Encuesta();
            //Act
            mockEncuesta.Setup(st => st.editarEncuesta(objEncuesta)).Returns(true);
            //Assert
            Assert.AreEqual(true, mockEncuesta.Object.editarEncuesta(objEncuesta));
        }

        [TestMethod]
        public void EliminarEncuesta()
        {
            Console.WriteLine("Eliminar encuesta");
            //Arrange
            Encuesta objEncuesta = new Encuesta();
            var mockEncuesta = new Mock<IEncuestaService>();
            //Act
            mockEncuesta.Setup(st => st.eliminarEncuesta(1)).Returns(true);
            //Assert
            Assert.AreEqual(true, mockEncuesta.Object.eliminarEncuesta(1));
        }

        [TestMethod]
        public void ListarEncuesta()
        {
            Console.WriteLine("Listar encuesta");
            //Arrange
            var mockEncuesta = new Mock<IEncuestaService>();
            //Act
            mockEncuesta.Setup(st => st.listaEncuestas()).Returns(new List<Encuesta>() { new Encuesta(), new Encuesta()});
            //Assert
            Assert.IsNotNull(mockEncuesta.Object.listaEncuestas());
        }
    }
}
